﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Butterfly : Enemy
    {
        public int health = 50;
        public int mana = 20;
        public float size = 10.50f;
        public float basedmg = 500.0f;
        public string name = "Mööp";
        public string element = "Fluff";
        public string battlecry = "Wääh";
        public bool alive = true;
        public bool hungry = true;
        public bool aggro = true;
        public bool sleeping = false;
    }
}