﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Add : Enemy
    {
        public int health = 70;
        public int mana = 15;
        public float size = 8.0f;
        public float basedmg = 20.0f;
        public string name = "Minion";
        public string element = "Ice";
        public string battlecry = "Meep Meep";
        public bool alive = true;
        public bool hungry = true;
        public bool aggro = true;
        public bool respawning = false;
    }
}