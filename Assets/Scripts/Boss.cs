﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Boss : Enemy
    {
        public int health = 500;
        public int mana = 250;
        public float size = 38.5f;
        public float basedmg = 40.0f;
        public string name = "Jurgen";
        public string element = "Dark";
        public string battlecry = "Grööhl";
        public bool alive = true;
        public bool hungry = true;
        public bool aggro = true;
        public bool hasAdds = true;

    }
}