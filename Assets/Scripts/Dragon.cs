﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Dragon : Enemy
    {
        public int health = 100;
        public int mana = 60;
        public float size = 55.5f;
        public float basedmg = 100.0f;
        public string name = "Puffles";
        public string element = "Candy";
        public string battlecry = "Muk-ya!";
        public bool alive = true;
        public bool hungry = false;
        public bool aggro = false;
        public bool flying = true;
    }
}